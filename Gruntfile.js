module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    sass: {
      // Leave development CSS expanded for debugging
      dev: {
        options: {
          outputStyle: 'expanded',
          sourceMap: false,
          sourceComments: true
        },
        files: {
          'assets/styles.css.liquid' : 'sass/styles.sass'
        }
      },
      // Compress CSS for production
      prod: {
        options: {
          outputStyle: 'compressed'
        },
        files: {
          'assets/styles.css.liquid' : 'sass/styles.sass'
        }
      }
    },
    watch: {
      options: {
        livereload: {
          key: grunt.file.read('.key/livereload.key'),
          cert: grunt.file.read('.key/livereload.crt')
        }
      },
      sass: {
        files: ['sass/**/*.sass', 'sass/**/*.scss'],
        tasks: ['sass:dev', 'run:liquid'],
        options: {
          livereload: false
        }
      },
      css: {
        files: ['build/*.css'],
        spawn: false
      }
    },
    connect: {
      server: {
        options: {
          livereload: true,
          protocol: 'https',
          port: 8000,
          keepalive: false,
          middleware: function(connect, options, middlewares) {
            middlewares.unshift(function(req, res, next) {
              res.setHeader('Access-Control-Allow-Origin', '*');
              res.setHeader('Access-Control-Allow-Methods', '*');

              next();
            });

            return middlewares;
          },
          base: {
            path: './',
          }
        }
      }
    },
    run: {
      liquid: {
        cmd: './bin/liquid',
      },
      theme: {
        cmd: 'theme',
        args: ['watch'],
        options: {
          wait: false
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-run');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-contrib-watch');

  // Tasks
  grunt.registerTask('default', ['connect', 'run:theme', 'watch']);
};
